(function ($) {

    "use strict";

    /* COLLAPSE NAVIGATION ON MOBILE AFTER CLICKING ON LINK */
	var ww = $(window).width();
	if (ww < 768) {
		$('.main-navigation a').on('click', function () {
			$(".navbar-toggle").click();
		});
	}

	$('#myModal').on('shown.bs.modal', function () {
	  $('#myInput').focus();
	});


$(document).ready(function(){

    //Check to see if the window is top if not then display button
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            //alert('test'+$('#scrollToTop').css("display"));
            $('#scrollToTop').css("display", "block");
            //alert('test'+$('#scrollToTop').css("display"));
            //$('.scrollToTop').style.display = "block";

           // $('.scrollToTop').fadeOut();
        } else {
            $('#scrollToTop').css("display", "none");
        }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });

});
/* =================================
===  STICKY NAV                 ====
=================================== */
    $('.main-navigation').onePageNav({
        scrollThreshold: 0.2, // Adjust if Navigation highlights too early or too late
        scrollOffset: 150, //Height of Navigation Bar
        filter: ':not(.external)',
        changeHash: true
    });

    /* NAVIGATION VISIBLE ON SCROLL */

    function mainNav() {
        var top = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
        if (top > 40) {
          $('.sticky-navigation').stop().animate({
            "opacity": '1',
            "top": '0'
        });
        } else {
          $('.sticky-navigation').stop().animate({
            "opacity": '1',
            "top": '0'
        });
        }
    }

    mainNav();
    $(window).scroll(function () {
        mainNav();
    });



/* =================================
===  Nivo Lightbox              ====
=================================== */
    $('#screenshots a').nivoLightbox({
        effect: 'fadeScale',
    });


/* =================================
===  CONTACT FORM               ====
=================================== */
    $("#contact").submit(function (e) {
        e.preventDefault();
        var fname = $("#cf-fname").val();
        var lname = $("#cf-lname").val();
        var email = $("#cf-email").val();
        var phone = $("#cf-phone").val();
        var optin = $("#cf-optin").val();
        var message = $("#cf-message").val();
        var dataString = 'fname=' + fname + '&lname='+ lname + '&email=' + email + '&message=' + message + '&phone=' + phone + '&optin=' + optin;

        function isValidEmail(emailAddress) {
            var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
            return pattern.test(emailAddress);
        }

        if ($("#cf-privacy").is(":checked")) {
			if (isValidEmail(email) && (message.length > 1) && (fname.length > 1) && (lname.length > 1)) {
				$.ajax({
					type: "POST",
					url: "sendmail.php",
					data: dataString,
					success: function () {
            $('.form-section1').hide();
						$('.success1').fadeIn(1000);
						$('.error1').fadeOut(500);
					}
				});
			}
			else {
				$('.error1').fadeIn(1000);
				$('.success1').fadeOut(500);
			}
        }else{
        	alert("You must agree to Baxters privacy policy.");
        }
        return false;
    });

    $("#contact2").submit(function (e) {
        e.preventDefault();
        var fname = $("#cf-fname2").val();
        var lname = $("#cf-lname2").val();
        var email = $("#cf-email2").val();
        var phone = $("#cf-phone2").val();
        var optin = $("#cf-optin2").val();
        var message = $("#cf-message2").val();
        var dataString = 'fname=' + fname + '&lname='+ lname + '&email=' + email + '&message=' + message + '&phone=' + phone + '&optin=' + optin;

        function isValidEmail(emailAddress) {
            var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
            return pattern.test(emailAddress);
        }

        if ($("#cf-privacy2").is(":checked")) {
			if (isValidEmail(email) && (message.length > 1) && (fname.length > 1) && (lname.length > 1)) {
				$.ajax({
					type: "POST",
					url: "sendmail.php",
					data: dataString,
					success: function () {
            $('.form-section2').hide();
						$('.success2').fadeIn(1000);
						$('.error2').fadeOut(500);
					}
				});
			}
			else {
				$('.error2').fadeIn(1000);
				$('.success2').fadeOut(500);
			}
        }else{
        	alert("You must agree to Baxters privacy policy.");
        }
        return false;
    });


/* =================================
===  EXPAND COLLAPSE            ====
=================================== */
    $('.expand-form').simpleexpand({
        'defaultTarget': '.expanded-contact-form'
    });


/* =================================
===  DOWNLOAD BUTTON CLICK SCROLL ==
=================================== */
    $('#cta-1, #cta-2, #cta-3, #cta-4, #cta-5').localScroll({
        duration: 1000
    });



/* =================================
===  SMOOTH SCROLL             ====
=================================== */

    // var scrollAnimationTime = 1200,
    //     scrollAnimation = 'easeInOutExpo';
    // $('a.scrollto').bind('click.smoothscroll', function (event) {
    //     event.preventDefault();
    //     var target = this.hash;
    //     $('html, body').stop().animate({
    //         'scrollTop': $(target).offset().top
    //     }, scrollAnimationTime, scrollAnimation, function () {
    //         window.location.hash = target;
    //     });
    // });

    var $root = $('html, body');
      $('a.scrollto').click(function() {
        var href = $.attr(this, 'href');
        $root.animate({
          scrollTop: $(href).offset().top
        }, 500, function(){
          window.location.hash = href;
        });
        return false;
      });

/* =================================
===  Bootstrap Internet Explorer 10 in Windows 8 and Windows Phone 8 FIX
=================================== */
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement('style');
        msViewportStyle.appendChild(
        document.createTextNode('@-ms-viewport{width:auto!important}'));
        document.querySelector('head').appendChild(msViewportStyle);
    }
})(jQuery);